
// klasa zawiera pola i metody definiujace karte do gry
public class Karta {
private char Kolor;
private int Figura;
public void setKolor(char K)
{
	Kolor=K;
}
public void setFigura(char F) throws Exception
{
	if(F>=48 && F<=57)
	this.Figura=(F-48);
	else if (F>=65 && F<=90)
	{
		if (F=='T')
			Figura=10;
		else if (F=='J')
			Figura=11;
		else if (F=='Q')
			Figura=12;
		else if (F=='K')
			Figura=13;
		else if (F=='A')
			Figura=14;
		else throw new Exception("Nie rozpoznano figury" + F + " w liscie figur karcianych");
		
	}
	else throw new Exception("Nie rozpoznano figury" + F + " w liscie figur karcianych");
}
public void set (char K, char F)
{
	
	Kolor=K;
	Figura=F;
}
public char getKolor ()
{
	return Kolor;	
}
public int getFigura()
{
	return Figura;
}
public boolean less(Karta ka)
{
	if(Figura < ka.getFigura())
		return true;
	return false;
}
public boolean more(Karta ka)
{
	if(Figura > ka.getFigura())
		return true;
	return false;
}
/*class nieprawidlowa_figura{
void nieprawildowa_figura()
{
	System.out.println("Nie rozpoznano podanej figury w liscie figur karcianych");
}
void nieprawildowa_figura(char F)
{
	System.out.println("Nie rozpoznano figury" + F + " w liscie figur karcianych");
}
}
Nie umiem tego użyć jak na razie :( 
*/
}
