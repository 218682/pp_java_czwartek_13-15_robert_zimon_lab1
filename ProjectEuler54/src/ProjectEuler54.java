import java.io.*;

public class ProjectEuler54 {

	public static void main(String[] args) {
		//Karta pom = new Karta();
		Gracz gracz1 = new Gracz();
		Gracz gracz2 = new Gracz();
		poker gra= new poker();
		char pom1;
		char pom2;
		int licznik=0;
		int linie=0;
		//char fi;
		File plik = new File("p054_poker.txt");
        try {
            BufferedReader br = new BufferedReader(new FileReader(plik));
            String linia = null;
            while ((linia = br.readLine()) != null) {
            	
            	gracz1.noweRozdanie();
            	gracz2.noweRozdanie();
            	for(int i=0; i<14; i++)
            	{
            		if(i==2 || i==5 || i==8 || i==11)
            			i++;
            		pom1 = linia.charAt(i);
            		//pom.setFigura(fi);
            		i++;
            		pom2 = linia.charAt(i);
            		//pom.setKolor(fi);
            		//gracz1.dodaj(pom);
            		gracz1.dodaj(pom1, pom2);
            	}

            	for(int i=15; i<29; i++)
            	{
            		if(i==17 || i==20 || i==23 || i==26)
            			i++;
            		pom1 = linia.charAt(i);
            		//pom.setFigura(fi);
            		i++;
            		pom2 = linia.charAt(i);
            		//pom.setKolor(fi)
            		//gracz2.dodaj(pom1);
            		gracz2.dodaj(pom1, pom2);
            	}
            	/*
        		System.out.println(gracz1.get(0).getFigura() + " " + gracz1.get(0).getKolor());
        		System.out.println(gracz1.get(1).getFigura() + " " + gracz1.get(1).getKolor());
        		System.out.println(gracz1.get(2).getFigura() + " " + gracz1.get(2).getKolor());
        		System.out.println(gracz1.get(3).getFigura() + " " + gracz1.get(3).getKolor());
        		System.out.println(gracz1.get(4).getFigura() + " " + gracz1.get(4).getKolor());

        		System.out.println(gracz2.get(0).getFigura() + " " + gracz2.get(0).getKolor());
        		System.out.println(gracz2.get(1).getFigura() + " " + gracz2.get(1).getKolor());
        		System.out.println(gracz2.get(2).getFigura() + " " + gracz2.get(2).getKolor());
        		System.out.println(gracz2.get(3).getFigura() + " " + gracz2.get(3).getKolor());
        		System.out.println(gracz2.get(4).getFigura() + " " + gracz2.get(4).getKolor());
        		*/
            	linie++;
            	System.out.println("zaladowano lini: " + linie);
            	if(gra.start(gracz1, gracz2))
            		licznik++;
            }    

    		br.close();  
        } catch (Exception e) {
            e.printStackTrace();

        }
        System.out.println("gracz pierwszy wygral " + licznik + " razy");
	}

}
