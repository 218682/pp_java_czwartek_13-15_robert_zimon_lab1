class punktacja
{
	public boolean prawda;  // czy uzyskano pokera, strita itp.
	public int punkt1;     // jaka najwyższa karta w kombinacji np. kareta z asów, gdu ful wartosc trojki
	public int punkt2;    // w wypadku dwoch par jest to wysokość niższej pary, gdy ful wartosc pary
	public int najwyzsza;// najwyzsza karta nie wchodzaca w sklad kombinacji
	public int pozostala; // zmienna do przechowywania pozostalych kart nie wchodzacych w sklad kombinacji
	public int pozostala2;
	punktacja()
	{
		prawda=false;
		punkt1=0;
		punkt2=0;
		najwyzsza=0;
		pozostala=0;
		pozostala2=0;
		
	}
	public boolean more(punktacja p2) //metoda porownujaca
	{
		if(this.prawda==true && p2.prawda==false)
			return true;
		if(this.prawda==true && p2.prawda==true)
		{
			
			if(this.punkt1>p2.punkt1)
				return true;
			if(this.punkt2>p2.punkt2)
				return true;
			if(this.najwyzsza>p2.najwyzsza)
				return true;
			if(this.pozostala>p2.pozostala)
				return true;
			if(this.pozostala2>p2.pozostala2)
				return true;
		}
		return false;
	}
}
public class poker {
	 // metoda zwraca prawde jezeli wygral pierwszy gracz a falsz jezeli wygral drugi 
	public boolean start (Gracz gracz1, Gracz gracz2)
	{
		if(PokerKrolewski(gracz1))
			return true;
		if(PokerKrolewski(gracz2))
			return false;
		
		if(Poker(gracz1).more(Poker(gracz2)))
			return true;
		if(Poker(gracz2).more(Poker(gracz1)))
			return false;
		
		if(Kareta(gracz1).more(Kareta(gracz2)))
			return true;
		if(Kareta(gracz2).more(Kareta(gracz1)))
			return false;
		
		if(Ful(gracz1).more(Ful(gracz2)))
			return true;
		if(Ful(gracz2).more(Ful(gracz1)))
			return false;
		
		if(Kolor(gracz1).more(Kolor(gracz2)))
			return true;
		if(Kolor(gracz2).more(Kolor(gracz1)))
			return false;
		
		if(Strit(gracz1).more(Strit(gracz2)))
			return true;
		if(Strit(gracz2).more(Strit(gracz1)))
			return false;
		
		if(Trojka(gracz1).more(Trojka(gracz2)))
			return true;
		if(Trojka(gracz2).more(Trojka(gracz1)))
			return false;
		
		if(DwiePary(gracz1).more(DwiePary(gracz2)))
			return true;
		if(DwiePary(gracz2).more(DwiePary(gracz1)))
			return false;
		
		if(Para(gracz1).more(Para(gracz2)))
			return true;
		if(Para(gracz2).more(Para(gracz1)))
			return false;
		
		if(Inne(gracz1).more(Inne(gracz2)))
			return true;
		if(Inne(gracz2).more(Inne(gracz1)))
			return false;
		
		System.out.println("cos tu nie gra");
		return false;
	}
	// ---------- sprawdzenie czy gracz posiada pokera krolewskiego ----------------------------------
	private boolean PokerKrolewski (Gracz gracz)
	{
		char kolor;
		try {
			if(gracz.get(0).getFigura()==10)
			{
				kolor=gracz.get(0).getKolor();
				if(gracz.get(1).getFigura()==11 && gracz.get(1).getKolor()==kolor)
					if(gracz.get(2).getFigura()==12 && gracz.get(2).getKolor()==kolor)
						if(gracz.get(3).getFigura()==13 && gracz.get(3).getKolor()==kolor)
							if(gracz.get(4).getFigura()==14 && gracz.get(4).getKolor()==kolor)
								{ System.out.println("poker krolewski!");return true; }
				return false;
			}
		} catch (Exception e) 	{	e.printStackTrace();	}
		return false;		
	}
	// -------------------------------------------------------------------------------------------------
	
	// ---------- sprawdzenie czy gracz posiada pokera -------------------------------------------------
	private punktacja Poker (Gracz gracz)
	{
		char kolor;
		int figura;
		punktacja p= new punktacja();
		try {
			figura=gracz.get(0).getFigura();
			kolor=gracz.get(0).getKolor();
			if((figura+4)>14)
			{
				p.prawda=false;
				return p;
			}
			if(gracz.get(4).getFigura()==14 && gracz.get(4).getKolor()==kolor)
			{
				if(figura==2)             //jeżeli jest as a pierwsza karta jest 2
					if(gracz.get(1).getFigura()==3 && gracz.get(1).getKolor()==kolor)
						if(gracz.get(2).getFigura()==4 && gracz.get(2).getKolor()==kolor)
							if(gracz.get(3).getFigura()==5 && gracz.get(3).getKolor()==kolor)
							{
								p.prawda=true;
								p.punkt1=5;
								System.out.println("Poker");
								return p;
							}
			}
			else if(gracz.get(1).getFigura()==(figura+1) && gracz.get(1).getKolor()==kolor)
					if(gracz.get(2).getFigura()==(figura+2) && gracz.get(2).getKolor()==kolor)
						if(gracz.get(3).getFigura()==(figura+3) && gracz.get(3).getKolor()==kolor)
							if(gracz.get(4).getFigura()==(figura+4) && gracz.get(4).getKolor()==kolor)
							{
								p.prawda=true;
								p.punkt1=figura+4;

								System.out.println("Poker");
								return p;
							}
		} catch (Exception e) 	{	e.printStackTrace();	}
		return p;	
	}
	// -------------------------------------------------------------------------------------------------
	
	// ---------- sprawdzenie czy gracz posiada karete -------------------------------------------------
	private punktacja Kareta (Gracz gracz)
	{
		int figura;
		punktacja p= new punktacja();
		try {
			
			figura=gracz.get(0).getFigura();
			if(gracz.get(1).getFigura()==figura)
			{
				if(gracz.get(2).getFigura()==figura)
					if(gracz.get(3).getFigura()==figura)
					{
						p.prawda=true;
						p.punkt1=figura;
						p.najwyzsza=gracz.get(4).getFigura();

						System.out.println("Kareta");
						return p;
					}
			}
			else
			{
				figura=gracz.get(1).getFigura();
				if(gracz.get(2).getFigura()==figura)
					if(gracz.get(3).getFigura()==figura)
						if(gracz.get(4).getFigura()==figura)
						{
							p.prawda=true;
							p.punkt1=figura;
							p.najwyzsza=gracz.get(0).getFigura();

							System.out.println("Kareta");
							return p;
						}
			}
		} catch (Exception e) {		e.printStackTrace();	}
		return p;
	}
	// -------------------------------------------------------------------------------------------------
	
	// ---------- sprawdzenie czy gracz posiada fulla --------------------------------------------------
	private punktacja Ful(Gracz gracz)
	{
		int figura1;
		int figura2;
		punktacja p= new punktacja();
		try {
			
			figura1=gracz.get(0).getFigura();
			if(gracz.get(1).getFigura()==figura1)
			{
				if(gracz.get(2).getFigura()==figura1) //gdy pierwsze 3 karty takie same
					{
						figura2=gracz.get(3).getFigura();
						if (gracz.get(4).getFigura()==figura2)
						{
							p.prawda=true;
							p.punkt1=figura1;
							p.punkt2=figura2;

							System.out.println("Ful");
							return p;
						}
					}
				else  //gdy ostatnie 3 karty takie same
				{
					figura2=gracz.get(2).getFigura();
					if (gracz.get(3).getFigura()==figura2)
						if (gracz.get(4).getFigura()==figura2)
						{
							p.prawda=true;
							p.punkt1=figura2;
							p.punkt2=figura1;

							System.out.println("Ful");
							return p;
						}
				}
					
			}
		} catch (Exception e) {		e.printStackTrace();	}
		return p;
	}
	// -------------------------------------------------------------------------------------------------
	
	// ---------- sprawdzenie czy gracz posiada kolor --------------------------------------------------
	private punktacja Kolor(Gracz gracz) //metoda zapisuje tylko 3 najwyzsze karty moze to powodowac bledy
	{
		char kolor;
		punktacja p= new punktacja();
		try {
				kolor=gracz.get(0).getKolor();
				if(gracz.get(1).getKolor()==kolor)
					if(gracz.get(2).getKolor()==kolor)
						if(gracz.get(3).getKolor()==kolor)
							if(gracz.get(4).getKolor()==kolor)
							{
							 p.prawda=true;
							 p.punkt1=gracz.get(4).getFigura();
							 p.punkt2=gracz.get(3).getFigura();
							 p.najwyzsza=gracz.get(2).getFigura();
							 p.pozostala=gracz.get(1).getFigura();
							 p.pozostala2=gracz.get(0).getFigura();

								System.out.println("Kolor");
							 return p;
							}
			} catch (Exception e) {		e.printStackTrace();	}
		return p;
	}
	//---------------------------------------------------------------------------------------------------
	
	// ---------- sprawdzenie czy gracz posiada strita --------------------------------------------------
	private punktacja Strit(Gracz gracz)
	{
		int figura;
		punktacja p= new punktacja();
		try {
			figura=gracz.get(0).getFigura();
			if(figura+4>14) //gdy nie ma szans na strita
			{
				return p;
			}
			if(gracz.get(4).getFigura()==14) //jeżeli jest as, a pierwsza karta jest 2
			{
				if(figura==2)
					if(gracz.get(1).getFigura()==3)
						if(gracz.get(2).getFigura()==4)
							if(gracz.get(3).getFigura()==5)
							{
								p.prawda=true;
								p.punkt1=5;

								System.out.println("Strit");
								return p;
							}
			}	
			else if(gracz.get(1).getFigura()==(figura+1))
					if(gracz.get(2).getFigura()==(figura+2))
						if(gracz.get(3).getFigura()==(figura+3))
							if(gracz.get(4).getFigura()==(figura+4))
							{
								p.prawda=true;
								p.punkt1=figura+4;

								System.out.println("Strit");
								return p;
							}
		} catch (Exception e) {		e.printStackTrace();	}
		return p;	
	}
	//---------------------------------------------------------------------------------------------------
	
	// ---------- sprawdzenie czy gracz posiada trojke --------------------------------------------------
	private punktacja Trojka(Gracz gracz)
	{
		punktacja p= new punktacja();
		try {
			if(gracz.get(0).getFigura()==gracz.get(1).getFigura()
					&& gracz.get(0).getFigura()==gracz.get(2).getFigura())
			{
				p.prawda=true;
				p.punkt1=gracz.get(0).getFigura();
				p.najwyzsza=gracz.get(4).getFigura();
				p.pozostala=gracz.get(3).getFigura();

				System.out.println("trojka");
				return p;
			}
			else if(gracz.get(1).getFigura()==gracz.get(2).getFigura() 
					&& gracz.get(1).getFigura()==gracz.get(3).getFigura())
			{
				p.prawda=true;
				p.punkt1=gracz.get(1).getFigura();
				p.najwyzsza=gracz.get(4).getFigura();
				p.pozostala=gracz.get(0).getFigura();
				System.out.println("trojka");
				return p;
			}
			else if(gracz.get(2).getFigura()==gracz.get(3).getFigura()
					&& gracz.get(2).getFigura()==gracz.get(4).getFigura())
			{
				p.prawda=true;
				p.punkt1=gracz.get(2).getFigura();
				p.najwyzsza=gracz.get(1).getFigura();
				p.pozostala=gracz.get(0).getFigura();
				System.out.println("trojka");
				return p;
			}
		} catch (Exception e) {		e.printStackTrace();	}
		return p;	
	}
	//---------------------------------------------------------------------------------------------------
	
	// ---------- sprawdzenie czy gracz posiada dwie pary------------------------------------------------
	private punktacja DwiePary(Gracz gracz)
	{
		punktacja p= new punktacja();
		try {
			if((gracz.get(0).getFigura())==(gracz.get(1).getFigura()))
			{
				if((gracz.get(2).getFigura())==(gracz.get(3).getFigura()))
				{
					p.prawda=true;
					p.punkt1=gracz.get(2).getFigura();
					p.punkt2=gracz.get(0).getFigura();    
					p.najwyzsza=gracz.get(4).getFigura();
					System.out.println("dwie pary");
					return p;
				}
				else if((gracz.get(3).getFigura())==(gracz.get(4).getFigura()))
				{
					p.prawda=true;
					p.punkt1=gracz.get(3).getFigura();
					p.punkt2=gracz.get(0).getFigura();    
					p.najwyzsza=gracz.get(2).getFigura();
					System.out.println("dwie pary");
					return p;
				}
			}
			else if((gracz.get(1).getFigura())==(gracz.get(2).getFigura()))
			{
				if((gracz.get(3).getFigura())==(gracz.get(4).getFigura()))
				{
					p.prawda=true;
					p.punkt1=gracz.get(3).getFigura();
					p.punkt2=gracz.get(1).getFigura();    
					p.najwyzsza=gracz.get(0).getFigura();
					System.out.println("dwie pary");
					return p;
				}
			}

		} catch (Exception e) {		e.printStackTrace();	}

		return p;
	}
	//---------------------------------------------------------------------------------------------------
	
	// ---------- sprawdzenie czy gracz posiada pare ----------------------------------------------------
	private punktacja Para(Gracz gracz)
	{
		punktacja p= new punktacja();
		try {
			if(gracz.get(0).getFigura()==gracz.get(1).getFigura())
			{
				p.prawda=true;
				p.punkt1=gracz.get(0).getFigura();   
				p.najwyzsza=gracz.get(4).getFigura();
				p.pozostala=gracz.get(3).getFigura();
				p.pozostala2=gracz.get(2).getFigura();
				System.out.println("para");
				return p;
			}
			if(gracz.get(1).getFigura()==gracz.get(2).getFigura())
			{
				p.prawda=true;
				p.punkt1=gracz.get(1).getFigura(); 
				p.najwyzsza=gracz.get(4).getFigura();
				p.pozostala=gracz.get(3).getFigura();
				p.pozostala2=gracz.get(0).getFigura();
				System.out.println("para");
				return p;
			}
			if(gracz.get(2).getFigura()==gracz.get(3).getFigura())
			{
				p.prawda=true;
				p.punkt1=gracz.get(2).getFigura();    
				p.najwyzsza=gracz.get(4).getFigura();
				p.pozostala=gracz.get(1).getFigura();
				p.pozostala2=gracz.get(0).getFigura();
				System.out.println("para");
				return p;
			}
			if(gracz.get(3).getFigura()==gracz.get(4).getFigura())
			{
				p.prawda=true;
				p.punkt1=gracz.get(3).getFigura();  
				p.najwyzsza=gracz.get(2).getFigura();
				p.pozostala=gracz.get(1).getFigura();
				p.pozostala2=gracz.get(0).getFigura();
				System.out.println("para");
				return p;
			}
		} catch (Exception e) {		e.printStackTrace();	}
		return p;
	}
	//---------------------------------------------------------------------------------------------------
	
	// ---------- sprawdzenie kart gdy nie ma zadnej kombincji-------------------------------------------
	private punktacja Inne(Gracz gracz)
	{
		punktacja p= new punktacja();
		try {
		p.prawda=true;
		p.punkt1=gracz.get(4).getFigura(); 
		p.punkt2=gracz.get(3).getFigura();
		p.najwyzsza=gracz.get(2).getFigura();
		p.pozostala=gracz.get(1).getFigura();
		p.pozostala2=gracz.get(0).getFigura();
		} catch (Exception e) {		e.printStackTrace();	}
		return p;
	}
}
