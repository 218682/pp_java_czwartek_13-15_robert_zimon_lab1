
public class Gracz {
	private Karta[] Karty;
	private int iloscKart;
	Gracz()
	{
		this.iloscKart=0;
		Karty= new Karta[5];
		try {
			for(int i=0; i<5; i++)
				Karty[i] = new Karta();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	void noweRozdanie()
	{
		iloscKart=0;
	}
	Karta get(int i) throws Exception
	{
		if(i>=0 && i<=4)
		return Karty[i];
		else throw new Exception("Podano zle polozenie karty");
	}
	void dodaj (Karta K)
	{
		Karty[iloscKart]=K;
		iloscKart++;
	
		if (iloscKart>4) 
			sortuj(); //sortuje karty w rece zeby latwiej sie gralo
	}
	void dodaj (char F, char K)
	{
		try {
			Karty[iloscKart].setFigura(F);
			Karty[iloscKart].setKolor(K);
		} catch (Exception e) {	e.printStackTrace(); }
		iloscKart++;
		if (iloscKart>4) 
			sortuj(); //sortuje karty w rece zeby latwiej sie gralo
	}
	void sortuj ()
	{
		Karta Pom;
		for(int n=iloscKart; n>0; n--)
			for(int i=0; i<iloscKart-1; i++)
				if(Karty[i].getFigura()>Karty[i+1].getFigura())
				{	
					Pom=Karty[i+1];
					Karty[i+1]=Karty[i];
					Karty[i]=Pom;
				}
	}
}
