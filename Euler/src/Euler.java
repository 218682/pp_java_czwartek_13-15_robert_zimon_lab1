
public class Euler {
	static int Fibonacci (int ile)
	{
		int wynik=0;
		int PomA=2;
		int PomB=1;
		int i=2;
		if(ile==0)
			return 0;
		if(ile==1)
			return 1;
		if (ile==2)
			return 2;
		while (i<ile)
		{
			wynik=PomA+PomB;
			PomB=PomA;
			PomA=wynik;
			//System.out.println(" wyraz ciagu " + wynik);
			i++;
		}
		return wynik;
	}
	static int FibonacciRek (int ile)
	{
		if(ile==1)
		return 1;
		else if (ile==2)
			return 2;
		else
			return FibonacciRek(ile-1)+FibonacciRek(ile-2);
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int wynik=0;
		int wyrazenie=0;
		int A=1;
		System.out.println("Suma parzystych wyrazen ciagu Fibonacciego:");
		while((wyrazenie=Fibonacci(A))<4000000)
		{		
			//System.out.println(" wyraz ciagu 1 " + wyrazenie);
			if (wyrazenie%2==0)
			wynik=wynik+wyrazenie;
			A=A+1;
		}
		System.out.println(wynik);

		A=1;
		wynik=0;
		while((wyrazenie=FibonacciRek(A))<4000000)
		{		
			//System.out.println(" wyraz ciagu 2 " + wyrazenie);
			if (wyrazenie%2==0)
			wynik=wynik+wyrazenie;
			A=A+1;
		}
		System.out.print(wynik);
	}

}
